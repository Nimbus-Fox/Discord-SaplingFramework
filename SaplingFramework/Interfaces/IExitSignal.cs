﻿using System;

namespace SaplingFramework.Interfaces {
    public interface IExitSignal {
        event EventHandler Exit;
    }
}