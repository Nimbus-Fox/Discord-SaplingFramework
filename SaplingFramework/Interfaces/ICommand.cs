﻿using System;
using Discord.WebSocket;

namespace SaplingFramework.Interfaces {
    internal interface ICommand {
        string Command { get; }
        void Execute(string[] bits, SocketUser user, ISocketMessageChannel channel, DateTimeOffset dateTime);
        string HandleHelp(string[] bits, SocketUser user);
        string Description { get; }
        bool AllowedInDm { get; }
        string PermissionNode { get; }
    }
}