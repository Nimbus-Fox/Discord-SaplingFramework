﻿using System;

namespace SaplingFramework.Enums {
    [Flags]
    public enum BlobKind {
        String,
        Number,
        Boolean,
        Decimal,
        List,
        Blob
    }
}