﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SaplingFramework {
    internal class LogData {
        public LogType Type;
        public string Message;
        public DateTime LoggedAt;
        public string ID;
    }
}
