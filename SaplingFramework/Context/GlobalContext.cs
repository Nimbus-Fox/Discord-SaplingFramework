﻿using System;
using System.Diagnostics;
using Discord.WebSocket;
using SaplingFramework.Classes;
using SaplingFramework.Interfaces;

namespace SaplingFramework.Context {
    internal static class GlobalContext {
        internal static Logger Logger;
        internal static Native.Native Native;
        internal static Instance Instance;
        internal static BotSettings Settings;
        internal static IExitSignal ExitSignal;
        internal static DiscordShardedClient Client;

        internal static bool DebugEnabled() {
            return Debugger.IsAttached;
        }
    }
}
