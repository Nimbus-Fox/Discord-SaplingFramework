﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SaplingFramework.Context {
    internal class DependencyContext {
        private static readonly Dictionary<Type, List<Type>> Dependencies = new Dictionary<Type, List<Type>>();

        internal static void Register<TBase, TType>() where TType : TBase {
            if (!Dependencies.ContainsKey(typeof(TBase))) {
                Dependencies.Add(typeof(TBase), new List<Type>());
            }

            Dependencies[typeof(TBase)].Add(typeof(TType));
        }

        internal static bool TryGetFirst<TBase, TType>(out TType output, params object[] arguments) where TType : TBase {
            output = default;

            var dependencies = GetAll<TBase>(arguments);

            var target = dependencies.FirstOrDefault(x => x.GetType().IsAssignableFrom(typeof(TType)));

            if (target is TType o) {
                output = o;
                return true;
            }

            return false;
        }

        internal static bool TryGetLast<TBase, TType>(out TType output, params object[] arguments) where TType : TBase {
            output = default;

            var dependencies = GetAll<TBase>(arguments);

            var target = dependencies.LastOrDefault(x => x.GetType().IsAssignableFrom(typeof(TType)));

            if (target is TType o) {
                output = o;
                return true;
            }

            return false;
        }

        internal static IEnumerable<TBase> GetAll<TBase>(params object[] arguments) {
            if (!Dependencies.ContainsKey(typeof(TBase))) {
                return new TBase[0];
            }

            return Dependencies[typeof(TBase)].Select(x => (TBase) Activator.CreateInstance(x, arguments));
        }
    }
}
