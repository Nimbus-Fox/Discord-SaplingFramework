﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SaplingFramework.Enums;
using SaplingFramework.Extensions;

namespace SaplingFramework.Context {
    internal static class TranslationContext {
        private static readonly Dictionary<string, Dictionary<string, string>> Translations = new Dictionary<string, Dictionary<string, string>>();

        internal static void Reload() {
            foreach (var item in Translations) {
                item.Value.Clear();
            }

            Translations.Clear();

            foreach (var file in GlobalContext.Native.GetFilesByExtension(".lang")) {
                using (var stream = GlobalContext.Native.LoadFile(file, FileAccess.Read)) {
                    using (var blob = stream.ReadBlob()) {
                        if (!blob.Contains("code")) {
                            continue;
                        }

                        var code = blob.GetString("code");

                        if (!Translations.ContainsKey(code)) {
                            Translations.Add(code, new Dictionary<string, string>());
                        }

                        var codes = Translations[code];

                        foreach (var entry in blob.Entries) {
                            if (!codes.ContainsKey(entry.Key)) {
                                if (entry.Value.Kind == BlobKind.String) {
                                    codes.Add(entry.Key, entry.Value.GetString());
                                    GlobalContext.Logger.WriteLine($"{code}.{entry.Key} translation registered", LogType.Debug);
                                }
                            }
                        }
                    }
                }
            }
        }

        internal static bool TryGetTranslation(out string output, string code, string languageCode, params object[] parameters) {
            output = null;

            if (!Translations.ContainsKey(languageCode)) {
                return false;
            }

            var codes = Translations[languageCode];

            if (!codes.ContainsKey(code)) {
                if (code == "info.missingTranslation") {
                    output = $"{code} is not available for {languageCode}";
                    return true;
                }

                return TryGetTranslation(out output, "info.missingTranslation", languageCode, parameters);
            }

            output = string.Format(codes[code], parameters);
            return true;
        }
    }
}
