﻿using System;
using System.Collections.Generic;
using System.Text;
using SaplingFramework.Classes;
using SaplingFramework.Classes.DatabaseRecord;
using SaplingFramework.Interfaces;

namespace SaplingFramework.Context {
    internal static class DatabaseContext {
        private static IDatabase Database;

        internal static void Init() {
            if (Database != null) {
                if (DependencyContext.TryGetFirst<IDatabase, IDatabase>(out var database)) {
                    Database = database;
                }
            }
        }
    }
}
