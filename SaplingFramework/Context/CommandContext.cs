﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Discord.WebSocket;
using SaplingFramework.Interfaces;

namespace SaplingFramework.Context {
    internal static class CommandContext {
        internal static IReadOnlyDictionary<string,ICommand> _commands;
        internal static void Init() {
            _commands = Helpers.GetTypesUsingBase<ICommand>().Select(type => (ICommand) Activator.CreateInstance(type))
                .ToDictionary(command => command.Command).OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);

            if (GlobalContext.DebugEnabled()) {
                foreach (var command in _commands) {
                    GlobalContext.Logger.WriteLine($"Command {command.Key} registered", LogType.Debug);
                }
            }

            GlobalContext.Logger.WriteLine($"{_commands.Count} commands registered");
        }

        internal static void HandleMessage(SocketMessage session) {
            if (session is SocketUserMessage == false) {
                return;
            }

            if (session.Author.IsBot) {
                return;
            }

            if (string.IsNullOrEmpty(session.Content)) {
                return;
            }

            var bits = session.Content.Split(' ');

            if (session.Content.StartsWith(GlobalContext.Settings.CommandChar)) {
                if (session.Channel is SocketDMChannel) {
                    foreach (var command in _commands.Values.Where(x => x.AllowedInDm)) {
                        if (session.Content.ToLower()
                            .StartsWith($"{GlobalContext.Settings.CommandChar}{command.Command}")) {
                            try {
                                command.Execute(bits, session.Author, session.Channel, session.CreatedAt);
                            } catch (Exception ex) {
                                GlobalContext.Logger.LogError(ex);
                            }
                            return;
                        }
                    }
                }

                if (_commands.ContainsKey(bits[0]
                    .Substring(1))) {
                    try {
                        _commands[bits[0].Substring(1)].Execute(bits, session.Author, session.Channel, session.CreatedAt);
                    } catch (Exception ex) {
                        GlobalContext.Logger.LogError(ex);
                    }
                    return;
                }


                session.Channel.SendMessageAsync($@"Sorry {session.Author.Mention} but I don't know that command.
Please use {GlobalContext.Settings.CommandChar}help to receive a list of commands");
            }
        }
    }
}
