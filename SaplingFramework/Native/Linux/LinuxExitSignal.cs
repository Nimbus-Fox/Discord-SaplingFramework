using System;
using System.Threading;
using SaplingFramework.Interfaces;
using Mono.Unix;
using Mono.Unix.Native;

namespace SaplingFramework.Native.Linux {
    internal class LinuxExitSignal : IExitSignal {
        public event EventHandler Exit;

        public LinuxExitSignal() {
            new Thread(() => {
                var signal = new UnixSignal(Signum.SIGINT);
                while (signal.WaitOne()) {
                    if (Exit != null) {
                        Exit(this, EventArgs.Empty);
                    }
                    break;
                }
            }).Start();
        }
    }
}