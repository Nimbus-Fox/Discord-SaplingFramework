using SaplingFramework.Context;

namespace SaplingFramework.Native.Linux {
    internal class Linux : Native {
        internal Linux(string[] args) : base(args) {
            GlobalContext.ExitSignal = new LinuxExitSignal();
        }

        internal override void Start() {
            GlobalContext.Instance.Start();
        }
    }
}