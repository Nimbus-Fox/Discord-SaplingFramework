﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using SaplingFramework.Context;

namespace SaplingFramework.Native.Windows {
    internal class Windows : Native {
        /// <inheritdoc />
        internal Windows(string[] args) : base(args) {
            GlobalContext.ExitSignal = new WindowsExitSignal();
        }

        /// <inheritdoc />
        internal override void Start() {
            using (var service = new BotService()) {
                if (!Debugger.IsAttached) {
                    ServiceBase.Run(service);
                } else {
                    GlobalContext.Instance.Start();
                }
            }
        }
    }
}
