﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using SaplingFramework.Classes;
using SaplingFramework.Context;

namespace SaplingFramework.Native.Windows { 
    internal class BotService : ServiceBase {

        protected override void OnStart(string[] args) {
            new Thread(() => { GlobalContext.Instance.Start(); }).Start();
        }

        protected override void OnStop() {
            GlobalContext.Instance.Stop();
        }
    }
}
