﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using SaplingFramework.Interfaces;

namespace SaplingFramework.Native.Windows {
    class WindowsExitSignal : IExitSignal {
        public event EventHandler Exit;

        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);

        public delegate bool HandlerRoutine(CtrlTypes CtrlType);

        public enum CtrlTypes {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }

        private HandlerRoutine m_hr;

        public WindowsExitSignal() {
            m_hr = new HandlerRoutine(ConsoleCtrlCheck);

            SetConsoleCtrlHandler(m_hr, true);
        }

        private bool ConsoleCtrlCheck(CtrlTypes ctrlType) {
            switch (ctrlType) {
                case CtrlTypes.CTRL_C_EVENT:
                case CtrlTypes.CTRL_BREAK_EVENT:
                case CtrlTypes.CTRL_CLOSE_EVENT:
                case CtrlTypes.CTRL_LOGOFF_EVENT:
                case CtrlTypes.CTRL_SHUTDOWN_EVENT:
                    if (Exit != null) {
                        Exit(this, EventArgs.Empty);
                    }
                    break;
                default:
                    break;
            }

            return true;
        }
    }
}
