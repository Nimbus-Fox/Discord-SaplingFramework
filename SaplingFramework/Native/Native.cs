﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using SaplingFramework.Classes;
using SaplingFramework.Context;
using SaplingFramework.Extensions;

namespace SaplingFramework.Native {
    internal abstract class Native {
        protected readonly string[] Arguments;
        protected static Dictionary<string, StreamWrapper> Streams { get; } = new Dictionary<string, StreamWrapper>();
        protected static List<string> Files { get; } = new List<string>();

        protected Native(string[] args) {
            Arguments = args;

            var dir = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory.FullName;

            if (Directory.GetCurrentDirectory() != dir) {
                Directory.SetCurrentDirectory(dir);
                GlobalContext.Logger.WriteLine("Detected working directory is not in the executable directory", LogType.Warning);
                GlobalContext.Logger.WriteLine($"Changing working directory to {dir}", LogType.Warning);
            }

            if (!Directory.Exists("databases")) {
                Directory.CreateDirectory("databases");
            }
        }

        internal abstract void Start();

        internal bool FileExists(string path) {
            return File.Exists(path);
        }

        internal Stream LoadFile(string path, FileAccess access = FileAccess.ReadWrite) {
            var toLow = path.ToLower();
            if (Streams.ContainsKey(toLow)) {
                var wrapper = Streams[toLow];

                if ((wrapper.CanRead || wrapper.CanWrite) && !wrapper.IsDisposed) {
                    wrapper.PosToStart();
                    return wrapper;
                }
            }

            var wrap = new StreamWrapper(new FileStream(toLow, FileMode.OpenOrCreate, access));

            if (Streams.ContainsKey(toLow)) {
                Streams[toLow] = wrap;
            } else {
                Streams.Add(toLow, wrap);
            }

            wrap.PosToStart();

            return wrap;
        }

        internal void SaveFile(string path, Stream data, bool disposeAfter = true) {
            var stream = LoadFile(path);
            stream.SetLength(0);
            data.PosToStart();
            data.CopyTo(stream);
            stream.Flush();

            if (disposeAfter) {
                stream.Dispose();
            }
        }

        internal void ScanDir() {
            Files.Clear();

            var dir = new DirectoryInfo("content");

            if (!dir.Exists) {
                dir.Create();
            }

            ScanDir(dir, dir.Name + "/");
        }

        private void ScanDir(DirectoryInfo dir, string prefix) {
            foreach (var file in dir.EnumerateFiles()) {
                GlobalContext.Logger.WriteLine($"{prefix}{file.Name} found!", LogType.Debug);
                Files.Add(prefix + file.Name);
            }

            foreach (var subDir in dir.EnumerateDirectories()) {
                ScanDir(subDir, prefix + subDir.Name + "/");
            }
        }

        internal IEnumerable<string> GetFilesByExtension(string extension) {
            return Files.Where(x => x.EndsWith(extension));
        }
    }
}