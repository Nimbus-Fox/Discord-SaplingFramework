﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Color = Discord.Color;

namespace SaplingFramework {
    internal static class Helpers {
        public static IEnumerable<Type> GetTypesUsingBase<T>() {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var asms = new List<Type>();

            foreach (var asm in assemblies) {
                try {
                    asms.AddRange(asm.GetTypes());
                } catch {
                    // ignore;
                }
            }

            return asms.Where(x => typeof(T).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract);
        }

        public static Color ColorFromHex(string hex) {
            var html = ColorTranslator.FromHtml(hex);

            return new Color(html.R, html.G, html.B);
        }
    }
}
