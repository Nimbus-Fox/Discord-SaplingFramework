﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using SaplingFramework.Context;
using SaplingFramework.Extensions;
using Newtonsoft.Json;

namespace SaplingFramework {
    [Flags]
    internal enum LogType {
        Info,
        Error,
        Fatal,
        Warning,
        Debug
    }

    internal class Logger {
        private string _id { get; }

        public Logger(string id) {
            _id = id;
        }

        private static ConcurrentQueue<LogData> _queue = new ConcurrentQueue<LogData>();

        public static bool HasQueue => !_queue.IsEmpty;

        private static bool _run = true;

        static Logger() {
            new Thread(() => {
                while (_run) {
                    try {
                        while (_queue.Count > 0) {
                            if (_queue.TryDequeue(out var data)) {
                                if (data.Type.HasFlag(LogType.Debug)) {
                                    if (!GlobalContext.DebugEnabled()) {
                                        continue;
                                    }
                                }

                                Console.Write($"[{data.LoggedAt:yyyy-MM-dd HH:mm:ss}]");

                                switch (data.Type) {
                                    case LogType.Warning:
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        break;
                                    case LogType.Error:
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        break;
                                    case LogType.Fatal:
                                        Console.ForegroundColor = ConsoleColor.Magenta;
                                        break;
                                    case LogType.Info:
                                        Console.ForegroundColor = ConsoleColor.Green;
                                        break;
                                    case LogType.Debug:
                                        Console.ForegroundColor = ConsoleColor.Cyan;
                                        break;
                                }

                                Console.Write($"[{LogTypeText(data.Type)}]");
                                Console.ResetColor();
                                Console.WriteLine($"[{data.ID}] {data.Message}");

                                if (!Directory.Exists("logs")) {
                                    Directory.CreateDirectory("logs");
                                }

                                using (var ms = new MemoryStream()) {
                                    if (GlobalContext.Native.FileExists($"logs/{data.LoggedAt:yyyy-MM-dd}.log")) {
                                        using (var fs =
                                            GlobalContext.Native.LoadFile($"logs/{data.LoggedAt:yyyy-MM-dd}.log")) {
                                            ms.WriteStringFromStart(fs.ReadAllText());
                                        }
                                    }

                                    ms.WriteString(
                                        $"[{data.LoggedAt:yyyy-MM-dd HH:mm:ss}][{LogTypeText(data.Type)}][{data.ID}] {data.Message}{Environment.NewLine}");
                                    GlobalContext.Native.SaveFile($"logs/{data.LoggedAt:yyyy-MM-dd}.log", ms);
                                }
                            }
                        }
                    } catch {
                        // ignore
                    }
                }
            }).Start();
        }

        public void WriteLine(string text, LogType type = LogType.Info) {
            var data = new LogData {ID = _id, LoggedAt = DateTime.Now, Message = text, Type = type};

            _queue.Enqueue(data);
        }

        public void LogError(Exception ex, LogType logType = LogType.Error, params object[] data) {
            var log = new Dictionary<string, object> {{"Exception", ex}, {"Data", data}};

            var fileName = $"{DateTime.UtcNow.Ticks}.error";
            using (var ms = new MemoryStream()) {
                ms.WriteStringFromStart(JsonConvert.SerializeObject(log, Formatting.Indented));
                GlobalContext.Native.SaveFile($"logs/{fileName}", ms);
            }

            WriteLine($"An error occured and was stored in {Path.GetFullPath($"logs/{fileName}")}", logType);
            WriteLine($"Exception: {ex.Message}", logType);
        }

        private static string LogTypeText(LogType type) {
            return Enum.GetName(typeof(LogType), type);
        }

        internal static void Dispose() {
            while (HasQueue) {
            }

            _run = false;
        }
    }
}