﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Xml.Linq;
using SaplingFramework.Context;
using SaplingFramework.Enums;
using ProtoBuf;
using SaplingFramework.Classes;

namespace SaplingFramework.Extensions {
    internal static class StreamExtensions {
        private static BinaryReader GetReader(Stream stream) {
            return new BinaryReader(stream, Encoding.UTF8, true);
        }

        private static BinaryWriter GetWriter(Stream stream) {
            return new BinaryWriter(stream, Encoding.UTF8, true);
        }

        public static void PosToStart(this Stream stream, bool reset = false) {
            try {
                stream.Seek(0L, SeekOrigin.Begin);

                if (reset) {
                    stream.SetLength(0);
                }
            } catch {
                // ignore
            }
        }

        public static byte[] ReadBytesFromStart(this Stream stream) {
            var buffer = new byte[1024];
            using (var ms = new MemoryStream()) {
                int read;
                stream.PosToStart();
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0) {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static byte[] ReadBytes(this Stream stream) {
            var buffer = new byte[1024];
            using (var ms = new MemoryStream()) {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0) {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static string ReadAllText(this Stream stream) {
            stream.PosToStart();

            string text;

            using (var sr = new StreamReader(stream, Encoding.UTF8, true, 1024, true)) {
                text = sr.ReadToEnd();
            }

            return text;
        }

        public static Blob ReadBlob(this Stream stream) {
            var blob = Blob.FromJson(stream.ReadAllText());

            if (blob.Contains("__inherit")) {
                var entry = blob.Entries["__inherit"];

                if (entry.Kind == BlobKind.String) {
                    if (GlobalContext.Native.FileExists(entry.GetString())) {
                        using (var subBlob = GlobalContext.Native.LoadFile(entry.GetString()).ReadBlob()) {
                            blob.MergeFrom(subBlob);
                        }
                    }
                } else if (entry.Kind == BlobKind.List) {
                    foreach (var item in entry.GetList()) {
                        if (item.Kind == BlobKind.String) {
                            if (GlobalContext.Native.FileExists(item.GetString())) {
                                using (var subBlob =
                                    GlobalContext.Native.LoadFile(item.GetString()).ReadBlob()) {
                                    blob.MergeFrom(subBlob);
                                }
                            }
                        }
                    }
                }
            }

            return blob;
        }

        public static Blob ReadBinaryBlob(this Stream stream) {
            stream.PosToStart();
            using (var compressedStream = new GZipStream(stream, CompressionMode.Decompress, true)) {
                return Serializer.Deserialize<Blob>(compressedStream);
            }
        }

        public static XDocument ReadXml(this Stream stream) {
            return XDocument.Parse(stream.ReadAllText());
        }

        public static void WriteBytesFromStart(this Stream stream, byte[] data) {
            stream.PosToStart();
            try {
                stream.SetLength(0);
            } catch {
                // ignore
            }
            using (var bw = GetWriter(stream)) {
                bw.Write(data);
            }
        }

        public static void WriteBytes(this Stream stream, byte[] data) {
            using (var bw = GetWriter(stream)) {
                bw.Write(data);
            }
        }

        public static void WriteString(this Stream stream, string data) {
            stream.WriteBytes(Encoding.UTF8.GetBytes(data));
        }

        public static void WriteStringFromStart(this Stream stream, string data) {
            stream.WriteBytesFromStart(Encoding.UTF8.GetBytes(data));
        }

        public static void WriteBinaryBlob(this Stream stream, Blob data) {
            stream.PosToStart(true);
            using (var compressedStream = new GZipStream(stream, CompressionLevel.Optimal, true)) {
                Serializer.Serialize(compressedStream, data);
            }
        }
    }
}
