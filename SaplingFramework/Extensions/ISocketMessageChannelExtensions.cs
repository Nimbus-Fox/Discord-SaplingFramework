using System.Collections.Generic;
using Discord.Rest;
using Discord.WebSocket;

namespace SaplingFramework.Extensions {
    internal static class ISocketMessageChannelExtensions {
        internal static bool TryGetSocketGuild(this ISocketMessageChannel channel, out SocketGuild guild) {
            guild = null;
            if (!(channel is SocketGuildChannel guildChan)) {
                return false;
            }
            
            guild = guildChan.Guild;
            return true;
        }

        internal static IReadOnlyCollection<SocketGuildUser> GetChannelUsers(this ISocketMessageChannel channel) {
            if (!(channel is SocketGuildChannel guildChan)) {
                return new List<SocketGuildUser>();
            }

            return guildChan.Users;
        }
    }
}