﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SaplingFramework.Classes {
    internal class BotSettings {
        internal string Token { get; }
        internal bool HasToken { get; }
        internal string CommandChar { get; private set; }
        internal bool SingleInstance { get; private set; }
        internal byte Shards { get; private set; }
        internal string DefaultLanguage { get; private set; }
        internal bool CommandsEnabled { get; private set; }

        public BotSettings(Blob blob) {
            if (blob.Contains("token")) {
                Token = blob.GetString("token");

                HasToken = !string.IsNullOrEmpty(Token);
            }
        }

        public virtual void Reload(Blob blob) {
            var c = blob.GetString("commandChar", "$");

            CommandChar = (string.IsNullOrEmpty(c) ? '$' : c[0]).ToString();

            DefaultLanguage = blob.GetString("defaultLanguage", "en-gb");

            SingleInstance = blob.GetBool("singleInstance", true);

            Shards = blob.GetByte("shards", 2);

            if (Shards > 6) {
                Shards = 6;
            }
        }
    }
}
