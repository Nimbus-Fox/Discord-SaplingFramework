﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SaplingFramework.Context;
using SaplingFramework.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProtoBuf;

namespace SaplingFramework.Classes {
    [ProtoContract]
    public class Blob : IDisposable {
        public IReadOnlyDictionary<string, BlobEntry> Entries => _entries;
        [ProtoMember(1)]
        private Dictionary<string, BlobEntry> _entries { get; } = new Dictionary<string, BlobEntry>();

        public void Dispose() {
            foreach (var entry in _entries) {
                if (entry.Value.Kind == BlobKind.Blob) {
                    entry.Value.GetBlob().Dispose();
                }
            }

            _entries.Clear();
            GC.Collect(GC.GetGeneration(this));
        }

        private void ValidateKey(string key) {
            if (!_entries.ContainsKey(key)) {
                _entries.Add(key, new BlobEntry());
            }
        }

        public void SetString(string key, string value) {
            ValidateKey(key);
            _entries[key].SetString(value);
        }

        public void SetBool(string key, bool value) {
            ValidateKey(key);
            _entries[key].SetBool(value);
        }

        public void SetNumber(string key, long value) {
            ValidateKey(key);
            _entries[key].SetNumber(value);
        }

        public void SetDouble(string key, double value) {
            ValidateKey(key);
            _entries[key].SetDouble(value);
        }

        public void SetList<T>(string key, List<T> value) {
            ValidateKey(key);
            _entries[key].SetList(value);
        }

        public void SetBlob(string key, Blob value) {
            ValidateKey(key);
            if (_entries[key].GetValue() is Blob blob) {
                blob.Dispose();
            }
            _entries[key].SetBlob(value);
        }

        public void SetGuid(string key, Guid value) {
            ValidateKey(key);
            _entries[key].SetString(value.ToString());
        }

        public Blob FetchBlob(string key) {
            ValidateKey(key);
            if (_entries[key].GetValue() == null) {
                _entries[key].SetBlob(new Blob());
            }
            return _entries[key].GetBlob();
        }

        public string GetString(string key) {
            return _entries[key].GetString();
        }

        public string GetString(string key, string _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetString(_default);
        }

        public bool GetBool(string key) {
            return _entries[key].GetBool();
        }

        public bool GetBool(string key, bool _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetBool(_default);
        }

        public long GetLong(string key) {
            return _entries[key].GetLong();
        }

        public long GetLong(string key, long _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetLong(_default);
        }

        public uint GetUInt(string key) {
            return _entries[key].GetUInt();
        }

        public uint GetUInt(string key, uint _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetUInt(_default);
        }

        public int GetInt(string key) {
            return _entries[key].GetInt();
        }

        public int GetInt(string key, int _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetInt(_default);
        }

        public byte GetByte(string key) {
            return _entries[key].GetByte();
        }

        public byte GetByte(string key, byte _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetByte(_default);
        }

        public decimal GetDecimal(string key) {
            return _entries[key].GetDecimal();
        }

        public decimal GetDecimal(string key, decimal _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetDecimal(_default);
        }

        public float GetFloat(string key) {
            return _entries[key].GetFloat();
        }

        public float GetFloat(string key, float _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetFloat(_default);
        }

        public double GetDouble(string key) {
            return _entries[key].GetDouble();
        }

        public double GetDouble(string key, double _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetDouble(_default);
        }

        public IEnumerable<BlobEntry> GetList(string key) {
            return _entries[key].GetList();
        }

        public IEnumerable<BlobEntry> GetList(string key, BlobEntry[] _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetList(_default);
        }

        public Blob GetBlob(string key) {
            return _entries[key].GetBlob();
        }

        public Blob GetBlob(string key, Blob _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetBlob(_default);
        }

        public Guid GetGuid(string key) {
            return Guid.Parse(_entries[key].GetString());
        }

        public Guid GetGuid(string key, Guid _default) {
            return Guid.Parse(!_entries.ContainsKey(key) ? _default.ToString()
                : _entries[key].GetString(_default.ToString()));
        }

        public override string ToString() {
            var data = new Dictionary<string, object>();
            GetJsonData(this, data);
            return JsonConvert.SerializeObject(data, Formatting.Indented);
        }

        public void Delete(string key) {
            if (_entries.ContainsKey(key)) {
                var entry = _entries[key];
                if (entry.Kind == BlobKind.Blob) {
                    entry.GetBlob().Dispose();
                }

                _entries.Remove(key);
            }
        }

        private static void GetJsonData(Blob level, IDictionary<string, object> current) {
            foreach (var entry in level.Entries.OrderBy(x => x.Key)) {
                if (entry.Value.Kind != BlobKind.Blob) {
                    current.Add(entry.Key, entry.Value.GetValue());
                } else {
                    var data = new Dictionary<string, object>();
                    GetJsonData(entry.Value.GetBlob(), data);
                    current.Add(entry.Key, data);
                }
            }
        }

        public void MergeFrom(Blob blob) {
            ReadJson(blob.ToString());
        }

        public bool Contains(string key) {
            return _entries.ContainsKey(key);
        }

        public Blob Clone() {
            var clone = new Blob();

            clone.ReadJson(ToString());

            return clone;
        }

        public static Blob FromJson(string json) {
            var blob = new Blob();

            blob.ReadJson(json);

            return blob;
        }

        public void ReadJson(string json) {
            try {
                var data = JsonConvert.DeserializeObject<JObject>(json);
                Parse(data, this);
            } catch when (!GlobalContext.DebugEnabled()) {
                throw new Exception("Invalid json was passed in. Please make sure the json is an object and not an array");
            }
        }

        private static void Parse(JObject data, Blob level) {
            foreach (var entry in data.Properties()) {
                if (entry.Value.Type == JTokenType.Object) {
                    Parse((JObject)entry.Value, level.FetchBlob(entry.Name));
                    continue;
                }

                if (entry.Value is JArray list) {
                    var pList = new List<BlobEntry>();

                    foreach (var item in list) {
                        var bEntry = new BlobEntry();

                        if (entry.Value.Type == JTokenType.Null) {
                            bEntry.SetString(null);
                            continue;
                        }

                        if (entry.Value.Type == JTokenType.String) {
                            bEntry.SetString(item.ToObject<string>());
                            continue;
                        }

                        if (entry.Value.Type == JTokenType.Integer) {
                            bEntry.SetNumber(item.ToObject<long>());
                            continue;
                        }

                        if (entry.Value.Type == JTokenType.Boolean) {
                            bEntry.SetBool(item.ToObject<bool>());
                            continue;
                        }

                        if (entry.Value.Type == JTokenType.Float) {
                            bEntry.SetDouble(item.ToObject<double>());
                            continue;
                        }
                    }

                    if (level.Contains(entry.Name)) {
                        var en = level.GetList(entry.Name);
                        pList.AddRange(en);
                        level.SetList(entry.Name, pList);
                    } else {
                        level.SetList(entry.Name, pList);
                    }
                    pList.Clear();
                    continue;
                }

                if (entry.Value.Type == JTokenType.Null) {
                    level.SetString(entry.Name, null);
                    continue;
                }

                if (entry.Value.Type == JTokenType.String) {
                    level.SetString(entry.Name, entry.Value.ToObject<string>());
                    continue;
                }

                if (entry.Value.Type == JTokenType.Integer) {
                    level.SetNumber(entry.Name, entry.Value.ToObject<long>());
                    continue;
                }

                if (entry.Value.Type == JTokenType.Boolean) {
                    level.SetBool(entry.Name, entry.Value.ToObject<bool>());
                    continue;
                }

                if (entry.Value.Type == JTokenType.Float) {
                    level.SetDouble(entry.Name, entry.Value.ToObject<double>());
                    continue;
                }
            }
        }

        public Blob FromObject(string key, object obj) {
            var blob = FetchBlob(key);
            blob.FromObject(obj);
            return blob;
        }

        public void FromObject(object obj) {
            ReadJson(JsonConvert.SerializeObject(obj));
        }

        public T ToObject<T>(string key) {
            return JsonConvert.DeserializeObject<T>(FetchBlob(key).ToString());
        }

        public T ToObject<T>() {
            return JsonConvert.DeserializeObject<T>(ToString());
        }

        public void Clear() {
            foreach (var entry in Entries.Keys.ToArray()) {
                Delete(entry);
            }
        }
    }
}
