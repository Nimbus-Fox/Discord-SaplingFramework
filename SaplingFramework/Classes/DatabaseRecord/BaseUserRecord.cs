﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SaplingFramework.Classes.DatabaseRecord {
    internal class BaseUserRecord {
        internal int ID { get; private set; }
        internal readonly ulong UserID;
        internal readonly IReadOnlyList<Guid> Groups;
        internal readonly IReadOnlyList<string> Permissions;
        internal readonly string LanguageCode;

        public BaseUserRecord() { }
    }
}
