﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using SaplingFramework.Context;

namespace SaplingFramework.Classes {
    internal class Instance {
        public Instance(string[] args) {
        }

        internal static event Action RegisterEvents;

        public void Start() {
            StartAsync().GetAwaiter().GetResult();
            Environment.Exit(0);
        }

        private async Task StartAsync() {
            var config = new DiscordSocketConfig {
                TotalShards = GlobalContext.Settings.Shards,
                AlwaysDownloadUsers = true
            };
            GlobalContext.Client = new DiscordShardedClient(config);
            
            RegisterEvents?.Invoke();

            GlobalContext.Client.Log += message => {
                GlobalContext.Logger.WriteLine(
                    $"{(message.Source == "Discord" ? "" : message.Source + " ")}{message.Message}");
                return Task.CompletedTask;
            };

            GlobalContext.Client.MessageReceived += message => {
                Context.CommandContext.HandleMessage(message);
                return Task.CompletedTask;
            };

            GlobalContext.Client.JoinedGuild += guild => {
                HandleGuildJoin(guild);
                return Task.CompletedTask;
            };

            await GlobalContext.Client.LoginAsync(TokenType.Bot, GlobalContext.Settings.Token);
            await GlobalContext.Client.StartAsync();

            GlobalContext.Client.LoggedOut += () => {
                GlobalContext.Logger.WriteLine("Closing sessions and stopping the program");

                while (Logger.HasQueue) {
                    Thread.Sleep(20);
                }

                GlobalContext.Client.Dispose();

                Environment.Exit(0);

                return Task.CompletedTask;
            };

            await Task.Delay(-1);
        }

        public void Stop() {
            GlobalContext.Client.LogoutAsync().Wait();
        }

        private static void HandleGuildJoin(SocketGuild guild) {
        }
    }
}