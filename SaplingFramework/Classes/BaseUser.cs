﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SaplingFramework.Classes.DatabaseRecord;

namespace SaplingFramework.Classes {
    internal class BaseUser {
        internal readonly ulong ID;
        internal List<Guid> Groups { get; }
        internal List<string> Permissions { get; }
        internal string LanguageCode { get; }
        
        [Obsolete("Only for ninject", true)]
        public BaseUser() {

        }

        internal BaseUser(ulong id) {
            ID = id;
            Groups = new List<Guid>();
            Permissions = new List<string>();
        }

        internal BaseUser(BaseUserRecord record) {
            ID = record.UserID;
            Groups = record.Groups.ToList();
            Permissions = record.Permissions.ToList();
            LanguageCode = record.LanguageCode;
        }

        internal void ToBlob(Blob blob) {
            blob.SetNumber("id", (long)ID);

            blob.SetList("groups", Groups);
            blob.SetList("permissions", Permissions);
        }
    }
}
