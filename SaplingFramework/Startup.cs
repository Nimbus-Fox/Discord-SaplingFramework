﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using SaplingFramework.Classes;
using SaplingFramework.Classes.DatabaseRecord;
using SaplingFramework.Context;
using SaplingFramework.Context.DatabaseType;
using SaplingFramework.Extensions;
using SaplingFramework.Interfaces;
using SaplingFramework.Native.Linux;
using SaplingFramework.Native.Windows;

namespace SaplingFramework {
    internal static class Startup {
        private static Func<Blob, BotSettings> _initSettings = InitSettings;

        internal static void Start(string botName, string[] args) {
            Console.CursorVisible = false;
            GlobalContext.Logger = new Logger(botName);

            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) => {
                if (eventArgs.ExceptionObject is Exception exception) {
                    if (eventArgs.IsTerminating) {
                        GlobalContext.Logger.LogError(exception, LogType.Fatal);
                    } else {
                        GlobalContext.Logger.LogError(exception);
                    }
                }
            };

            AppDomain.CurrentDomain.ProcessExit += (sender, eventArgs) => { GlobalContext.Instance.Stop(); };

            DependencyContext.Register<BaseUser, BaseUser>();
            GlobalContext.Logger.WriteLine(
                $"{DependencyContext.GetAll<BaseUser>().First().GetType().FullName} is bound to {typeof(BaseUser).FullName}",
                LogType.Debug);

            DependencyContext.Register<BaseUserRecord, BaseUserRecord>();
            GlobalContext.Logger.WriteLine(
                $"{DependencyContext.GetAll<BaseUserRecord>().First().GetType().FullName} is bound to {typeof(BaseUserRecord).FullName}",
                LogType.Debug);

            DependencyContext.Register<IDatabase, BlobDatabase>();
            GlobalContext.Logger.WriteLine(
                $"{DependencyContext.GetAll<IDatabase>().First().GetType().FullName} is bound to {typeof(IDatabase).FullName}",
                LogType.Debug);
#if NETCOREAPP
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                GlobalContext.Logger.WriteLine("Windows OS detected");
                GlobalContext.Native = new Windows(args);
            } else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
                GlobalContext.Logger.WriteLine("Linux OS detected");
                GlobalContext.Native = new Linux(args);
            } else {
                GlobalContext.Logger.WriteLine("Operating system not supported", LogType.Error);
                Console.ReadKey();
                Environment.Exit(0);
            }
#else
            switch (Environment.OSVersion.Platform) {
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.Win32NT:
                case PlatformID.WinCE:
                    GlobalContext.Logger.WriteLine("Windows OS detected");
                    GlobalContext.Native = new Windows(args);
                    break;
                case PlatformID.Unix:
                    GlobalContext.Logger.WriteLine("Unix OS detected");
                    GlobalContext.Native = new Linux(args);
                    break;
            }
#endif

            GlobalContext.ExitSignal.Exit += (sender, eventArgs) => { GlobalContext.Instance.Stop(); };

            Reload();

            if (!GlobalContext.Settings.HasToken) {
                GlobalContext.Logger.WriteLine(
                    "No token was found in config.json. Please check that it has a \"token\" property", LogType.Error);
                Environment.Exit(1);
            }

            GlobalContext.Instance = new Instance(args);

            DatabaseContext.Init();
            CommandContext.Init();

            GlobalContext.Logger.WriteLine("Starting discord bot");
            GlobalContext.Native.Start();
        }

        internal static void Reload() {
            GlobalContext.Logger.WriteLine("Scanning content directory");
            GlobalContext.Native.ScanDir();
            GlobalContext.Logger.WriteLine("Finished scanning the content directory");

            if (!GlobalContext.Native.FileExists("./config.json")) {
                GlobalContext.Logger.WriteLine("config.json is missing", LogType.Error);
                Console.ReadKey();
                Environment.Exit(1);
            }

            GlobalContext.Logger.WriteLine("Loading Settings");

            using (var stream = GlobalContext.Native.LoadFile("./config.json", FileAccess.Read)) {
                using (var blob = stream.ReadBlob()) {
                    if (GlobalContext.Settings == null) {
                        GlobalContext.Settings = _initSettings(blob);
                    }

                    GlobalContext.Settings.Reload(blob);
                }
            }

            TranslationContext.Reload();
            PermissionsContext.Reload();

            GlobalContext.Logger.WriteLine("Finished loading settings");
        }

        internal static void SetSettingsInitializer(Func<Blob, BotSettings> init) {
            _initSettings = init;
        }

        private static BotSettings InitSettings(Blob blob) {
            return new BotSettings(blob);
        }
    }
}