﻿using System;
using System.Collections.Generic;
using System.Text;
using Discord;
using Discord.WebSocket;
using SaplingFramework.Context;
using SaplingFramework.Interfaces;

namespace SaplingFramework.Commands {
    internal class HelpCommand : ICommand {
        /// <inheritdoc />
        public string Command => "command.help";

        /// <inheritdoc />
        public void Execute(string[] bits, SocketUser user, ISocketMessageChannel channel, DateTimeOffset dateTime) {
            var target = user.GetOrCreateDMChannelAsync().Result;
            var typing = target.EnterTypingState();
            var embed = new EmbedBuilder();

            embed.Color = Color.Orange;
            embed.Title = "Kitsune Bot commands";

            foreach (var command in CommandContext._commands.Values) {
                embed.AddField($"{GlobalContext.Settings.CommandChar}{command.Command}", command.Description);
            }

            target.SendMessageAsync(null, false, embed.Build());
            typing.Dispose();
        }

        /// <inheritdoc />
        public string HandleHelp(string[] bits, SocketUser user) {
            return "";
        }

        /// <inheritdoc />
        public string Description =>
            $"Shows this list of commands. Using {GlobalContext.Settings.CommandChar}help <command> <args> will display extra help";

        /// <inheritdoc />
        public bool AllowedInDm => true;

        public string PermissionNode => "public.help";
    }
}
