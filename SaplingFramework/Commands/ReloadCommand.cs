﻿using System;
using System.Linq;
using Discord.WebSocket;
using SaplingFramework.Context;
using SaplingFramework.Interfaces;

namespace SaplingFramework.Commands {
    class ReloadCommand : ICommand {
        /// <inheritdoc />
        public string Command => "command.reload";

        /// <inheritdoc />
        public void Execute(string[] bits, SocketUser user, ISocketMessageChannel channel, DateTimeOffset dateTime) {
            Startup.Reload();
        }

        /// <inheritdoc />
        public string HandleHelp(string[] bits, SocketUser user) {
            return "";
        }

        /// <inheritdoc />
        public string Description => "command.reload.description";

        /// <inheritdoc />
        public bool AllowedInDm => false;

        /// <inheritdoc />
        public string PermissionNode => "admin.reload";
    }
}
