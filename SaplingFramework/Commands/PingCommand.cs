﻿using System;
using System.Collections.Generic;
using System.Text;
using Discord.WebSocket;
using SaplingFramework.Interfaces;

namespace SaplingFramework.Commands {
    internal class PingCommand : ICommand {
        /// <inheritdoc />
        public string Command => "command.ping";

        /// <inheritdoc />
        public void Execute(string[] bits, SocketUser user, ISocketMessageChannel channel, DateTimeOffset dateTime) {
            channel.SendMessageAsync($"{(DateTime.Now - dateTime.DateTime).Milliseconds} ms");
        }

        /// <inheritdoc />
        public string HandleHelp(string[] bits, SocketUser user) {
            return null;
        }

        /// <inheritdoc />
        public string Description => "command.ping.description";

        /// <inheritdoc />
        public bool AllowedInDm => true;

        /// <inheritdoc />
        public string PermissionNode => "admin.ping";
    }
}
